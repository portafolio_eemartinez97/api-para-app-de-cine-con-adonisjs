'use strict'

const UserHook = exports = module.exports = {}
const Customer = use('App/Models/Customer');

UserHook.setCustomer = async (userInstance) => {
  let customer = new Customer();
  userInstance.customer().save(customer)
}
