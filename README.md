# Backend proyecto cine (API)

En este proyecto se desarolló una aplicación que simula la reserva de entradas 
de cine (asientos), en este caso esta parte corresponde al back-end (API) de la 
aplicación la cual se trabajó con AdonisJs

## Temas que destacan dentro del proyecto

* Migraciones
* Factorias
* Relaciones
* Seeds
* Protección de rutas con Middlewares y JWT (GET, POST, PUT)
* Controladores
* Validaciones
* Entre Otros